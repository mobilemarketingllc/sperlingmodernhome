<?php
/**
 * Empty cart page
 *
 * 	Modifications
 * 	- Add container around empty cart page
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<section class="afs-empty-cart">
	<?php
		wc_print_notices();

		/**
		 * @hooked wc_empty_cart_message - 10
		 */
		do_action( 'woocommerce_cart_is_empty' );

		if ( wc_get_page_id( 'shop' ) > 0 ) :
	?>

		<p class="return-to-shop">
			<a class="button wc-backward" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
				<?php _e( 'Return to shop', 'woocommerce' ) ?>
			</a>
		</p>

	<?php endif; ?>
</section>
