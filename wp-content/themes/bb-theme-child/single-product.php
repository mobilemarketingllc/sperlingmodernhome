<?php get_header(); ?>

<div class="container">
	<div class="row">
	        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				
                <div class="col-md-6 col-xs-12"  style="text-align: center;">
      
      
  
      <h2  style="text-align: center;"><a href="<?php echo get_permalink(); ?>" ><strong> <?php echo  get_field('product_title') ?></strong></a></h2>
      <div style="text-align: center;"><?php echo  get_field('product_description') ?></div>
      <p style="text-align: center;"> product_brand: <?php the_field('product_brand'); ?></p>
      <p style="text-align: center;"> product_collection: <?php the_field('product_collection'); ?></p>
      <p style="text-align: center;">is_featured: <?php the_field('is_featured'); ?></p>


<?php 
       $product_image = get_field('product_image');
      $size = 'medium'; // (thumbnail, medium, large, full or custom size)
      $thumb = $product_image['sizes'][ $size ];

?>
     
<img src="<?php if($thumb!='') {echo $thumb;} else{ echo '';} ?>"/>

  
<h3 class="product-name" style="text-align: center;"><strong><?php the_title(); ?></h3></strong>

</div>

  <?php endwhile; ?>
<?php else: ?>
       <h1>No posts here!</h1>
<?php endif; ?>
<div id="expandInfo<?php echo $j;?>" class="collapseInfo" style="clear:both;"></div>


<?php if( have_rows('specifications') ): ?>


<?php if( have_rows('specifications') ): ?>

<ul>

<?php while( have_rows('specifications') ): the_row(); ?>

    <li>sub_field_1 = <?php the_sub_field('spec_title'); ?>, sub_field_2 = <?php the_sub_field('spec_value'); ?>, etc</li>
    
 
    
<?php endwhile; ?>

</ul>

<?php
global $post;
$flooringtype = $post->post_type; 
$meta_values = get_post_meta( get_the_ID() );

$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');
$pdp_order_sample = get_option('pdp_order_sample ');
$ordersamplereplace = get_option('ordersamplereplace');
$ordersamplereplaceurl = get_option('ordersamplereplaceurl');
$ordersampletext = get_option('ordersampletext');



?>
</div></div></div>
                                        <div class="clearfix"></div>
                                    <div class=" button-wrapper-default">                                        
										<?php if( $getcouponbtn == 1){  ?>
											<a href="<?php if($getcouponreplace == 1){ echo $getcouponreplaceurl;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<?php if($getcouponreplace ==1){ echo $getcouponreplacetext;}else{ echo 'GET COUPON'; }?>
											</a>
            							<?php } ?>
                                        <a href="/contact-us/" class="button contact-btn">CONTACT US</a>

					<?php if($pdp_get_finance != 1 || $pdp_get_finance == '' ){?>						
					
					<a href="<?php if($getfinancereplace ==1){ echo $getfinancereplaceurl;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button">
					
					<?php if($getfinancereplace =='1'){ echo $getfinancetext;}else{ echo 'Get Financing'; } ?></a>	
					
					<?php if($pdp_order_sample != 1 || $pdp_order_sample == '' ){?>						
					
					<a href="<?php if($ordersamplereplace ==1){ echo $ordersamplereplaceurl;}else{ echo '/order-sample/'; } ?>" class="finance-btn button">
						
					<?php if($ordersamplereplace =='1'){ echo $ordersampletext;}else{ echo 'order sampl'; } ?></a>	
										<?php } ?>
					<?php } ?>
                                    </div>
                                </div>
                            </div>
                                

<?php endif; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
</div>
</div>
</div>
</div>
<?php get_footer(); ?>
?>





























