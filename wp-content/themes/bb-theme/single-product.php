<?php /* Template Name: Product */ ?>



<?php get_header(); ?>

<?php 
$product_title = get_field('product_title');
$product_description = get_field('product_description');
$product_image = get_field('product_image');
$product_brand = get_field('product_brand');
$product_collection = get_field('product_collection');
$is_featured = get_field('is_featured');    
                   
?>

<div class="container product-detail">
	<div class="row">
    		<?php $args = array('post_type' => 'product', 'posts_per_page' =>'-1'); ?>
   		 <?php $loop = new WP_Query($args); ?>
   		 <?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();  ?>
       <div class="col-md-6 col-xs-12"  style="text-align: center;">
                <h2  style="text-align: center;"><strong> <?php echo  get_field('product_title') ?></strong></h2>
		<div style="text-align: center;"><?php echo  get_field('product_description') ?></div>
                <p style="text-align: center;"> product_brand: <?php the_field('product_brand'); ?></p>
		<p style="text-align: center;"> product_collection: <?php the_field('product_collection'); ?></p>
		<p style="text-align: center;">is_featured: <?php the_field('is_featured'); ?></p>


         <?php 
         	$product_image = get_field('product_image');
                $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                $thumb = $product_image['sizes'][ $size ];
	
	?>
               
<img src="<?php if($thumb!='') {echo $thumb;} else{ echo '';} ?>"/>

            
<h3 class="product-name" style="text-align: center;"><strong><?php the_title(); ?></h3></strong>

</div>

    	<?php endwhile; ?>
        <?php else: ?>
                 <h1>No posts here!</h1>
        <?php endif; ?>
        <div id="expandInfo<?php echo $j;?>" class="collapseInfo" style="clear:both;"></div>
        <?php wp_reset_postdata(); ?>
	</div>
</div>
</div>
</div>
<?php get_footer(); ?>
?>